from pymongo import MongoClient
import pymongo
import gridfs, ffmpeg, os
import topics, json, uuid
import asyncio, signal
import schedule 
print(os.environ['MONGODB_URI'])
client = MongoClient(os.environ['MONGODB_URI'])
db = client['kaas']
fs = gridfs.GridFS(db, collection='recordings')
class MessageCache():
    def __init__(self):
        self.cache = []
        self.limit = 32
    def mark_handled(self, message):
        self.cache.append(message['msgId'])
        if len(self.cache) > self.limit:
            self.cache.pop(0)
    def is_handled(self, message):
        print(message)
        return message['msgId'] in self.cache
cache = MessageCache()
def convert_file(input_file, output_file):
    try:
        os.remove(output_file)
    except:
        pass
    stream = ffmpeg.input(input_file)
    stream = ffmpeg.output(stream, output_file)
    ffmpeg.run(stream)
def get_file(file, output_directory):
    output_file = os.path.join(output_directory, file.filename)
    with open(output_file, 'wb') as out:
        with fs.get(file._id) as from_gfs:
            out.write(from_gfs.read())
    return os.path.join(output_directory, file.filename)
def update_file(file, input_file):
    with open(input_file, 'rb') as input:
        new_meta = file.metadata
        new_meta['converted'] = True
        fs.put(input, metadata=new_meta, filename=file.filename, contentType="video/mp4")
    fs.delete(file._id)
def cleanup_recordings():
    unconverted = fs.find({"metadata.converted":False})
    for f in unconverted:
        downloaded_file = get_file(f, "/tmp")
        new_file_name = downloaded_file.replace(".h264", ".mp4")
        convert_file(downloaded_file, new_file_name)
        update_file(f, new_file_name)
        topic = topics.RECORDING_CONVERTED.format({'feederId':f.metadata['feederId'], 'feedingId':f.metadata['feedingId']})
        publish(client, topic, payload)
def handle_recordings(client, payload):
    f = fs.find_one({"metadata.feederId":payload['feederId'], "metadata.feedingId":payload['feedingId'], "metadata.converted":False})
    downloaded_file = get_file(f, "/tmp")
    new_file_name = downloaded_file.replace(".h264", ".mp4")
    convert_file(downloaded_file, new_file_name)
    update_file(f, new_file_name)
    topic = topics.RECORDING_CONVERTED.format(payload)
    publish(client, topic, payload)

from gmqtt import Client as MQTTClient

STOP = asyncio.Event()
def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe(topics.RECORDING_STORED.topic, qos=2)
    


def on_message(client, topic, payload, qos, properties):
    if topics.RECORDING_STORED.matches(topic):
        payload = json.loads(payload)
        if not cache.is_handled(payload):
            handle_recordings(client, payload)
            cache.mark_handled(payload)
        else:
            print(f"message {payload['msgId']} is already handled")

def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()
def publish(client, topic, payload):
    payload['msgId'] = str(uuid.uuid4())
    client.publish(topic, json.dumps(payload))
    
async def main(broker_host):
    client = MQTTClient("recording-processor")

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)
    await STOP.wait()
    await client.disconnect()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    schedule.every(5).minutes.do(cleanup_recordings)

    host = os.environ['MQTT_BROKER_HOST']

    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    loop.run_until_complete(main(host))