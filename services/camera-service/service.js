
const bodyParser = require("body-parser")
const express = require("express")
const app = express()
const server = require("http").Server(app)
const mongoose = require("mongoose")
const multer  = require('multer')
const GridFsStorage = require("multer-gridfs-storage");
const topics = require("./topics.js")
const mqtt = require("mqtt")
const logger = require("./logger.js")("camera-service")
const uuid = require('uuid-random')
let gfs; 

class MQTTClient {
    constructor(client) {
        this.client = client
        this.client.setMaxListeners(255)
    }
    on(event, callback) {
        return this.client.on(event, callback)
    }
    subscribe(topic) {
        return this.client.subscribe(topic)
    }
    async matchTopic(topic, payload, regex, callback) {
        var params = regex.exec(topic)
        if (params) {
            await callback(null, params, JSON.parse(payload))
        }
    }
    async publish(topic, params, payload) {
        logger.info(`publishing message to ${topic.format(params)}:: ${JSON.stringify(payload)}`)
        return await this.client.publish(topic.format(params), JSON.stringify(payload))
    }
    async handleTopic(topic, payload, regex, handler) {
        var self = this
        await this.matchTopic(topic, payload, regex, async (err, params, payload) => {
            logger.info(`client ${params.feederId} sent message on ${topic}`)
            try {
                await handler(self, params, payload)
            } catch (err) {
                logger.error(`failure processing event for topic [${topic}]`)
                logger.error(err)
            }
        })
    }

}

const storage = new GridFsStorage({ url : "mongodb://mongo/kaas", 
    file: function(req, file) {
        return {
            filename: file.originalname,
            bucketName: "recordings",
            metadata: {
                feederId: req.body.feederId,
                feedingId: req.body.feedingId,
                captureStart: req.body.captureStart,
                captureEnd: req.body.captureEnd,
                timestamp: new Date(),
                length: req.body.length, 
                converted: false
            }
        }
    }
})


const upload = multer({storage})

const Recording = require("@mcfitz2/kaas-schemas").Recording


mongoose.connect("mongodb://mongo/kaas", {
    useNewUrlParser: true
})
const db = mongoose.connection



db.once("open", () => {
    gfs = new mongoose.mongo.GridFSBucket(db.db, {
        bucketName: "recordings"
    });
    const client = new MQTTClient(mqtt.connect("mqtt://broker"))
    app.use(bodyParser.json())


    app.post('/recording/create', upload.single('file'), async function (req, res, next) {
        await client.publish(topics.RECORDING_STORED, {feederId:req.body.feederId, feedingId: req.body.feedingId}, {feederId:req.body.feederId, feedingId: req.body.feedingId, timestamp: new Date(), length: req.body.length, msgId: uuid()})
        res.send("OK")
    })
    app.get('/recordings', (req,res) =>{
        gfs.find().toArray((err, files) =>{
          if(!files || files.length === 0) {
              return res.status(404).json({
              err: "No file exist"
              });
          }
          files.map((file) => {

          })
          return res.json(files);
        });
    });
    app.get('/recordings/:filename', (req, res) => {
        gfs.findOne({filename: req.params.filename}, (err, file) => {
            if (file) {
                var readStream = gfs.createReadStream({
                _id: file._id,
                root: 'recordings'
                })
                readStream.pipe(res)
            } else {
                res.send('File not found')
            }
        })
    })
      
    app.use(function (err, req, res, next) {
        console.log({msg: "in error handler", err: err})
        if (res.headersSent) {
            return next(err)
        }
    
        res.status(500)
        res.json({
            status: "failed",
            message: err.message
        })
    
    })
    server.listen(8080, "0.0.0.0");
});