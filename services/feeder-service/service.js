const mqtt = require("async-mqtt");
const mongoose = require("mongoose")
const handlers = require("./handlers");
const topics = require("./topics.js")
const bodyParser = require("body-parser")
const express = require("express")
const logger = require("./logger.js")("feeder-service")
class MQTTClient {
    constructor(client) {
        this.client = client
        this.client.setMaxListeners(255)
    }
    on(event, callback) {
        return this.client.on(event, callback)
    }
    subscribe(topic) {
        return this.client.subscribe(topic)
    }
    async matchTopic(topic, payload, regex, callback) {
        var params = regex.exec(topic)
        if (params) {
            await callback(null, params, JSON.parse(payload))
        }
    }
    async publish(topic, params, payload) {
        logger.info(`publishing message to ${topic.format(params)}:: ${JSON.stringify(payload)}`)
        return await this.client.publish(topic.format(params), JSON.stringify(payload))
    }
    async handleTopic(topic, payload, regex, handler) {
        var self = this
        await this.matchTopic(topic, payload, regex, async (err, params, payload) => {
            logger.info(`client ${params.feederId} sent message on ${topic}`)
            try {
                await handler(self, params, payload)
            } catch (err) {
                logger.error(`failure processing event for topic [${topic}]`)
                logger.error(err)
            }
        })
    }

}


mongoose.connect("mongodb://mongo/kaas", {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
const db = mongoose.connection
db.once("open", async () => {
    const app = express()
    const server = require("http").Server(app)
    const client = new MQTTClient(mqtt.connect("mqtt://"+process.env['MQTT_BROKER_HOST'], {clientId:"feeder-service"}))
    app.use(bodyParser.json())

    client.on("connect", async () => {
        await client.subscribe(topics.FEED_COMPLETE.topic)
        await client.subscribe(topics.NEED_INIT.topic)
        await client.subscribe(topics.FEED_FAILED.topic)
        await client.subscribe(topics.STARTUP.topic)
        await client.subscribe(topics.HEARTBEAT.topic)
    })


    

    client.on("message", async (topic, payload) => {
        await client.handleTopic(topic, payload, topics.HEARTBEAT, handlers.handleHeartbeat)
        await client.handleTopic(topic, payload, topics.FEED_COMPLETE, handlers.handleFeedingComplete)
        await client.handleTopic(topic, payload, topics.FEED_FAILED, handlers.handleFeedingFailed)
        await client.handleTopic(topic, payload, topics.STARTUP, handlers.handleStartup)
        await client.handleTopic(topic, payload, topics.NEED_INIT, handlers.handleNeedInit)
        await client.handleTopic(topic, payload, topics.SCHEDULE_SET, handlers.handleScheduleSet)
    })
    var jobs = await require("./tasks")(app, client)
    logger.info(jobs)
    require("./routes")(app, client)
    server.listen(8080, "0.0.0.0");

});
