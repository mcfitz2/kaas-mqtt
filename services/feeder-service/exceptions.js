class FeederNotConnected extends Error {
    constructor() {
        super()
        this.name = "FeederNotConnected"
        this.message = "Feeder is not connected"
    }
}
class FeederNotResponding extends Error {
    constructor() {
        super()
        this.name = "FeederNotResponding"
        this.message = "Feeder is not responding"
    }
}

module.exports = {
    FeederNotResponding:FeederNotResponding,
    FeederNotConnected:FeederNotConnected
}