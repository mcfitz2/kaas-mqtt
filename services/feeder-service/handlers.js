const Feeder = require("@mcfitz2/kaas-schemas").Feeder
const Feeding = require("@mcfitz2/kaas-schemas").Feeding
const topics = require("./topics.js")
const logger = require("./logger.js")("feeder-service")

async function handleHeartbeat(client, params, payload) {
    var original = await Feeder.findOneAndUpdate({
        feederId: params.feederId
    }, {
        feederId: params.feederId,
        lastSeen: new Date(),
        lost: false
    }, {
        upsert: true,
        setDefaultsOnInsert: true
    })
    if (original.lost) {
        await client.publish(topics.FOUND, params, {})
    }
}


async function handleFeedingComplete(client, params, payload) {
    await Feeder.findOneAndUpdate({ feederId: params.feederId }, { lastSeen: new Date(), lastFeeding: new Date() })
    //await Feeding.create({ feederId: params.feederId, timestamp: new Date(), cups: payload.cups })
    //await nClient.sendNotification(msg.feederId, "Feeding completed", `Your pet was fed ${msg.cups} cups of food at ${new Date()}`)
}

async function handleFeedingFailed(client, params, payload) {
    
}

async function handleScheduleSet(client, params, payload) {
    await Feeder.findOneAndUpdate({
        feederId: params.feederId
    }, {
        schedule: payload.jobs
    })
    //await nClient.sendNotification(msg.feederId, "Schedule set", `Your pet's feeding schedule was updated to ${JSON.stringify(msg.jobs, false, 4)}`)
}
async function handleNeedInit(client, params, payload) {
    let feeder = await Feeder.findOne({feederId: params.feederId})
    logger.info(`feeder ${params.feederId} is asking for init`)
    await client.publish(topics.CLIENT_INIT, {feederId:params.feederId}, feeder)
}

function handleStartup(client, params, payload) {
}

module.exports = {
    handleHeartbeat: handleHeartbeat,
    handleFeedingComplete: handleFeedingComplete,
    handleScheduleSet: handleScheduleSet,
    handleNeedInit: handleNeedInit,
    handleStartup: handleStartup,
    handleFeedingFailed: handleFeedingFailed
}