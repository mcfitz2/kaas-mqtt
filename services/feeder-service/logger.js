var bunyan = require('bunyan');
var bmqtt = require('bunyan-mqtt');

module.exports = function (name) {
    return bunyan.createLogger({
        name: name,
        streams: [{
            level: 'info',
            type: 'raw',
            stream: bmqtt({ topic: '/logs', port: 1883, host: 'broker' })
        },{
            name: "console",
            stream: process.stderr,
            level: "debug"
          }
        ]
    })
}