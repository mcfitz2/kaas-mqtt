async function waitFor(client, resolveTopic, rejectTopic, timeout = 10000) {
    return new Promise(async (resolve, reject) => {
        let to = setTimeout(() => {
            reject()
        }, timeout)
        await client.subscribe(resolveTopic)
        client.on("message", (t, p) => {
            if (resolveTopic == t) {
                clearTimeout(to)
                resolve({topic:t, payload:p})
            }
        })
        await client.subscribe(rejectTopic)
        client.on("message", (t, p) => {
            if (rejectTopic == t) {
                clearTimeout(to)
                reject()
            }
        })
            
        
    })
}

module.exports = {
    waitFor:waitFor,
}