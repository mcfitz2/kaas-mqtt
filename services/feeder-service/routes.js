const topics = require("./topics.js")
const exceptions = require("./exceptions.js")
const Feeder = require("@mcfitz2/kaas-schemas").Feeder
const Feeding = require("@mcfitz2/kaas-schemas").Feeding
const utils = require("./utils.js")
const logger = require("./logger.js")("feeder-service")

module.exports = (app, client) => {
    app.get("/feeders", async (req, res, next) => {
        try {
            let feeders = await Feeder.find({})
            res.json(feeders)
        } catch (err) {
            next(err)
        }
    })
    app.get("/feeders/claimedBy/:userId", async (req, res, next) => {
        try {
            let feeders = await Feeder.find({ claimedByUser: req.params.userId })
            res.json(feeders)
        } catch (err) {
            next(err)
        }
    })
    app.post("/feeders/:id/feed", async (req, res, next) => {
        var feeding = await Feeding.create({ feederId: req.params.id, timestamp: new Date(), cups: req.body.cups, status: "InProgress" })
        await client.publish(topics.FEED, { feederId: req.params.id }, { feederId: req.params.id, cups: req.body.cups, feedingId: feeding._id })
        try {
            var ret = await utils.waitFor(client, topics.FEED_COMPLETE.format({ feederId: req.params.id }), topics.FEED_FAILED.format({ feederId: req.params.id }), 20000)
            logger.info("got complete message")
            feeding = await Feeding.findByIdAndUpdate(feeding._id, { status: "Finished" }, { new: true })
            await client.publish(topics.FEEDING_COMPLETE, { feederId: req.params.id, feedingId: feeding._id}, { feederId: req.params.id, cups: req.body.cups, feedingId: feeding._id })
            res.json(feeding)
        } catch (err) {
            logger.info("failed")
            feeding = await Feeding.findByIdAndUpdate(feeding._id, { status: "Failed" }, { new: true })
            await client.publish(topics.FEEDING_FAILED, { feederId: req.params.id, feedingId: feeding._id}, { feederId: req.params.id, cups: req.body.cups, feedingId: feeding._id })
            res.status(500).json(feeding)
        }
    })

    app.post("/feeders/:id/claim", async (req, res, next) => {
        try {
            let feeder = await Feeder.findOneAndUpdate({
                feederId: req.params.id
            }, {
                claimedByUser: req.body.userId
            }, { 
                new: true 
            })
            res.json(feeder)
        } catch(err) {
            next(err)
        }
    })
    app.get("/feeders/:id/schedule", async (req, res, next) => {
        try {
            let feeder = await Feeder.findOne({
                feederId: req.params.id
            })
            res.json(feeder.schedule)
        } catch (err) {
            next(err)
        }
    })
    app.get("/feeders/:id/feedings", (req, res, next) => {
        Feeding.find({
            feederId: req.params.id
        }).sort({ timestamp: -1 }).then((feedings) => {
            res.json(feedings)
        }).catch((e) => {
            next(e)
        })
    })

    app.get("/feedings", (req, res, next) => {
        Feeding.find({}).sort({ timestamp: -1 }).then((feedings) => {
            res.json(feedings)
        }).catch((e) => {
            next(e)
        })
    })

    app.put("/feeders/:id/schedule", async (req, res, next) => {
        await client.publish(topics.SET_SCHEDULE, { feederId: req.params.id }, req.body)
        try {
            await utils.waitFor(client, topics.SCHEDULE_SET.format({ feederId: req.params.feederId }), 20000)
        } catch (err) {
            throw new exceptions.FeederNotResponding()
        }
        success(res)
    })
    app.delete("/feeders/:id", (req, res, next) => {
        Feeder.findOneAndDelete({
            feederId: req.params.id
        }).then(() => {
            success(res)

        }).catch((e) => {
            next(e)
        })
    })
    app.get("/feeders/:id", (req, res, next) => {
        Feeder.findOne({
            feederId: req.params.id
        }).then((feeder) => {
            res.json(feeder)
        }).catch((e) => {
            next(e)
        })
    })

    app.get("/feedings/:id", (req, res, next) => {
        Feedings.findById(req.params.id).then((feeder) => {
            res.json(feeder)
        }).catch((e) => {
            next(e)
        })
    })

    app.use(function (err, req, res, next) {
        logger.info({ msg: "in error handler", err: err })
        if (res.headersSent) {
            return next(err)
        }
        if (err instanceof exceptions.FeederNotConnected) {
            res.status(500)
            res.json({
                "status": "failed",
                "error": err.name,
                "message": err.message
            })
        } else if (err instanceof exceptions.FeederNotResponding) {
            res.status(500)
            res.json({
                "status": "failed",
                "error": err.name,
                "message": err.message
            })
        } else {
            res.status(500)
            res.json({
                status: "failed",
                message: err.message
            })
        }
    })
}
