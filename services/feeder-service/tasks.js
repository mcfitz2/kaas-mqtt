const Feeder = require("@mcfitz2/kaas-schemas").Feeder
const topics = require("./topics.js")
const schedule = require("node-schedule")
const logger = require("./logger.js")("feeder-service")

module.exports = async function (app, client) {
    logger.info("Registering lost feeder job")
    const lostJob = schedule.scheduleJob('0,30 * * * * *', async function(){
        logger.info("Checking for lost feeders")
        var feeders = await Feeder.find({})
        var checkTime = new Date()
        for await (const feeder of feeders) {

            if (!feeder.lost && (checkTime - feeder.lastSeen) > 30*1000) {
                feeder.lost = true
                await feeder.save()
                await client.publish(topics.LOST, {feederId: feeder.feederId}, {})
            }
        }
    });
    return {lost: lostJob}
}