# Import this library
import apprise


slack_url = "slack://xoxb-898198891780-2242288297905-T5EiwUWa3rMStFo3fwXXR7wP/#alerts-catfeeder"
# create an Apprise instance and assign it to variable `apobj`
apobj = apprise.Apprise()
apobj.add(slack_url)



from gmqtt import Client as MQTTClient
import os
import topics, json, uuid
import asyncio, signal

STOP = asyncio.Event()
def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe(topics.UPDATE_SERVICE.topic, qos=2)


        
        

def on_message(client, topic, payload, qos, properties):
    if topics.SERVICE_UPDATED.matches(topic):
        payload = json.loads(payload)
        apobj.notify(body=f"{payload['service']} updated to version {payload['current_version']}", title="Service Updated")
    elif topics.SERVICE_UPDATE_CHECK.matches(topic):
        payload = json.loads(payload)
        apobj.notify(body=f"Checking for updates for {payload['service']}", title="Service Updated")

def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()
def publish(client, topic, payload):
    payload['msgId'] = str(uuid.uuid4())
    client.publish(topic, json.dumps(payload))

async def main(broker_host):
    client = MQTTClient("agent")

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)
    await STOP.wait()
    await client.disconnect()

if __name__ == '__main__':
    handle_old_instances()
    loop = asyncio.get_event_loop()

    host = os.environ['MQTT_BROKER_HOST']

    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    loop.run_until_complete(main(host))

