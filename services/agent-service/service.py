from gmqtt import Client as MQTTClient, client
import os
import topics, json, uuid
import asyncio, signal
import datetime
import asyncio
import logging
from aiohttp import http, web
STOP = asyncio.Event()
loop = asyncio.get_event_loop()

class Task(object):
    GLOBAL_TASK_TIMEOUT = 10
    def __init__(self, task_type, **kwargs):
        self.task_id = str(uuid.uuid4())
        self.status = "created"
        self.task_type = task_type
        self.metadata = kwargs
    def start(self, **kwargs):
        self.status = "started"
        self.metadata.update(kwargs)
        self.metadata['started_time'] = datetime.datetime.now().isoformat()
        print(f"Scheduling timeout [{self.task_id}]")
        loop.call_later(Task.GLOBAL_TASK_TIMEOUT, self.fail)

    def finish(self, **kwargs):
        self.status = "finished"
        self.metadata.update(kwargs)
        self.metadata['finished_time'] = datetime.datetime.now().isoformat()

    def update(self, **kwargs):
        self.status = "finished"
        self.metadata.update(kwargs)
        self.metadata['updated_time'] = datetime.datetime.now().isoformat()
    def fail(self, **kwargs):
        print(f"failing task [{self.task_id} if not already finished. status = [{self.status}]")
        if self.status != "finished":
            self.status = "failed"
            self.metadata.update(**kwargs)
            self.metadata['finished_time'] = datetime.datetime.now().isoformat()

    def dict(self):
        return self.__dict__
class TaskManager(object):
    def __init__(self):
        self.tasks = {}
    def get_task(self, task_id):
        return self.tasks[task_id]
    def create_task(self, task_type, **kwargs):
        task = Task(task_type, **kwargs)
        self.tasks[task.task_id] = task
        return task
    def start_task(self, task_id, **kwargs):
        self.tasks[task_id].start(**kwargs)
    def finish_task(self, task_id, **kwargs):
        self.tasks[task_id].finish(**kwargs)

def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe(topics.SERVICE_UPDATE_CHECK.topic, qos=2)
    client.subscribe(topics.SERVICE_UP_TO_DATE.topic, qos=2)
      
def on_message(client, topic, payload, qos, properties):
    payload = json.loads(payload)
    if topics.SERVICE_UPDATED.matches(topic):
        task = task_manager.get_task(payload['task_id'])
        print("Got update message")
        print(f"finishing task {task.task_id}")
        task_manager.finish_task(task.task_id)
    elif topics.SERVICE_UP_TO_DATE.matches(topic):
        task = task_manager.get_task(payload['task_id'])
        print("Got up to date message")
        print(f"finishing task {task.task_id}")
        task_manager.finish_task(task.task_id)

def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()

def publish(client, topic, payload):
    payload['msgId'] = str(uuid.uuid4())
    client.publish(topic, json.dumps(payload))


task_manager = TaskManager()
client = MQTTClient("agent-service")
app = web.Application()

async def update_service(request):
    pass

async def update_feeder_service(request):
    feeder_id = request.match_info['feeder_id']
    service = request.match_info['service']
    task = task_manager.create_task("update_service")
    task.start(service=service)
    publish(client, topics.UPDATE_SERVICE.format({"feederId":feeder_id, "service":service}), {"feederId":feeder_id, "service":service, "task_id": task.task_id})
    return web.json_response(task.dict())

async def query_task(request):
    task_id = request.match_info['task_id']
    task = task_manager.get_task(task_id)
    return web.json_response(task.dict())

app.router.add_route('POST', '/update/{service}', update_service)
app.router.add_route('POST', '/feeders/{feeder_id}/update/{service}', update_feeder_service)
app.router.add_route('GET', '/tasks/{task_id}', query_task)


async def start_http():
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 8080)
    await site.start()
    
async def start_mqtt(broker_host):

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)
    await STOP.wait()
    await client.disconnect()

   
    
if __name__ == '__main__':
    host = os.environ['MQTT_BROKER_HOST']

    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)
    loop.create_task(start_mqtt(host))
    loop.create_task(start_http())
    try:
        loop.run_forever()
    except:
        print("loop exit")
