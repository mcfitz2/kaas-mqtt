#!/bin/bash
cd "$(dirname "$0")"
cd ../clients
cp -a ../common/kaas-topics-node/topics.js feeder-client/
docker-compose build
docker-compose push