import requests, pprint

feeder_service_url = "http://localhost:7777"
agent_service_url = "http://localhost:7779"
all_services = ['kaas-agent', 'kaas-feeder-client', 'kaas-camera-client']

def feeder_route(path):
    return feeder_service_url + path

def agent_route(path):
    return agent_service_url + path

def get_feeders():
    r = requests.get(feeder_route("/feeders"))
    return r.json() 

def update_service(feeder_id, service):
    def invoke_update_service(feeder_id, service):
        r = requests.post(agent_route(f"/feeders/{feeder_id}/update/{service}"))
        print(r.content)
        return r.json()
    task = invoke_update_service(feeder_id, service)
    while task['status'] not in  ["failed", "finished", "canceled"]:
        task = query_task(task['task_id'])
    pprint.pprint(task)

def update_all_feeders(service):
    feeders = get_feeders()
    for feeder in feeders:
        update_service(feeder['feederId'], service)

def update_all_feeders_all_services():
    feeders = get_feeders()
    for feeder in feeders:
        for service in all_services:
            update_service(feeder['feederId'], service)

def update_feeder_all_services(feeder_id):
    for service in all_services:
        update_service(feeder_id, service)

def query_task(task_id):
    return requests.get(agent_route(f"/tasks/{task_id}")).json()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--feeder-id", action='store', help="feeder id")
    parser.add_argument("-s", "--service", action='store', help="service name")
    parser.add_argument("--update", action='store_true', help="Update service")

    args = parser.parse_args()
    if args.update:
        if args.feeder_id == 'all' and args.service == 'all':
            update_all_feeders_all_services()
        elif args.feeder_id == 'all':
            update_all_feeders(args.service)
        elif args.service == 'all':
            update_feeder_all_services(args.feeder_id)
        else:
            update_service(args.feeder_id, args.service)