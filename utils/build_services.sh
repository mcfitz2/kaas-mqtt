
#!/bin/bash
cd "$(dirname "$0")"
cd ../services
cp -a ../common/kaas-topics-node/topics.js camera-service/
cp -a ../common/kaas-topics-node/topics.js feeder-service/
docker-compose build
docker-compose push