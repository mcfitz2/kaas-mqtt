#!/bin/bash
cd "$(dirname "$0")"
cd ../clients
docker-compose build 
docker-compose push 
mosquitto_pub -h localhost -t /feeder/feeder-1/services/kaas-feeder-client/update -m '{"service":"kaas-feeder-client"}'
mosquitto_pub -h localhost -t /feeder/feeder-1/services/kaas-camera-client/update -m '{"service":"kaas-camera-client"}'
mosquitto_pub -h localhost -t /feeder/feeder-1/services/kaas-agent/update -m '{"service":"kaas-agent"}'
