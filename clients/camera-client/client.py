import os, time, requests, asyncio, signal, json, datetime, uuid


feederId = os.environ['FEEDER_ID']

BEFORE_TRIGGER_SECONDS = 20
pi = os.environ['IS_PI'] == "true"

class MessageCache():
    def __init__(self):
        self.cache = []
        self.limit = 32
    def mark_handled(self, message):
        self.cache.append(message['msgId'])
        if len(self.cache) > self.limit:
            self.cache.pop(0)
    def is_handled(self, message):
        try:
            return message['msgId'] in self.cache
        except KeyError:
            print(f"msgId not present in message {message}")
cache = MessageCache()
if pi:
    import picamera
else:
    sample_file = "sample.h264"
    import shutil


service_url = os.environ['CAMERA_SERVICE_URL']
create_recording_route = "/recording/create"
storage_path = "/tmp/recordings"




if not os.path.exists(storage_path):
    os.mkdir(storage_path)

if pi:
    camera = picamera.PiCamera()
    stream = picamera.PiCameraCircularIO(camera, seconds=BEFORE_TRIGGER_SECONDS)
    camera.start_recording(stream, format='h264')


def create_feed_recording(path, feederId, feedingId, capture_start, capture_end, length):
    files = {'file': (f'{feederId}.{feedingId}.h264', open(path, 'rb'), 'video/h264', {'Expires': '0'})}
    data = {"feederId": feederId, "feedingId": feedingId, "length":length, "captureStart":capture_start, "captureEnd":capture_end}
    print(service_url+create_recording_route, data)
    r = requests.post(service_url+create_recording_route, data=data, files=files)
    print(r.status_code, r.content)

def snippet(client, feederId, feedingId, length=10):
    print("capturing video")
    capture_start = datetime.datetime.now() - datetime.timedelta(seconds=BEFORE_TRIGGER_SECONDS)
    if pi:
        print("on real RPI, capturing new video")
        camera.wait_recording(length)
        capture_end = datetime.datetime.now()
        stream.copy_to(os.path.join(storage_path, f'{feederId}.{feedingId}.h264'))
    else:
        print("simulated, using sample video")
        time.sleep(length)
        capture_end = datetime.datetime.now()
        shutil.copy(sample_file, os.path.join(storage_path, f'{feederId}.{feedingId}.h264'))
    publish(client, f'/feeder/{feederId}/feeding/{feedingId}/recorded', {'feederId':feederId, 'feedingId': feedingId, 'length':length+BEFORE_TRIGGER_SECONDS})
    create_feed_recording(os.path.join(storage_path, f'{feederId}.{feedingId}.h264'), feederId, feedingId, capture_start, capture_end, length+BEFORE_TRIGGER_SECONDS)
    return "OK"


from gmqtt import Client as MQTTClient

STOP = asyncio.Event()
def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe(f'/feeder/{feederId}/#', qos=0)


def on_message(client, topic, payload, qos, properties):
    if topic == f'/feeder/{feederId}/feedComplete':
        payload = json.loads(payload)
        if not cache.is_handled(payload):
            snippet(client, payload['feederId'], payload['feedingId'])
            cache.mark_handled(payload)
        else:
            print(f"message {payload['msgId']} is already handled")

def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()
def publish(client, topic, payload):
    payload['msgId'] = str(uuid.uuid4())
    client.publish(topic, json.dumps(payload))

async def main(broker_host):
    client = MQTTClient("camera-client")

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)
    client.publish(f'/feeder/{feederId}/camera/ready', json.dumps({'feederId':feederId}))
    await STOP.wait()
    await client.disconnect()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    host = os.environ['MQTT_BROKER_HOST']

    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    loop.run_until_complete(main(host))
