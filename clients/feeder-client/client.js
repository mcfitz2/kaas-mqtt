const mqtt = require("async-mqtt")
const winston = require('winston')
const { v4: uuidv4 } = require('uuid');
const fs = require("fs")
const EventEmitter = require('events');
const uuid = require('uuid-random');

class Dispenser extends EventEmitter {
    constructor(servoPin, limitPin, cupsToClicks) {
        super()
        this.logger = new winston.createLogger(myWinstonOptions)
        this.servoPin = servoPin
        this.limitPin = limitPin
        this.cupsToClicks = cupsToClicks
        if (wpi) {
            wpi.wiringPiSetupGpio();
            wpi.pinMode(servoPin, wpi.PWM_OUTPUT)
            wpi.pullUpDnControl(limitPin, wpi.PUD_UP)
            wpi.pinMode(limitPin, wpi.INPUT)
            //set the PWM mode to milliseconds stype
            wpi.pwmSetMode(wpi.PWM_MODE_MS)
            wpi.pwmSetClock(192)
            wpi.pwmSetRange(2000)
        }
        this._stop()
        this.emit("ready")
    }
    _delay(t) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve();
            }, t);
        });
    }
    _forward() {
        this.logger.info("setting motor to forward")
        if (wpi) {
            wpi.pwmWrite(this.servoPin, 175)
        }
        this.emit("motorForward")
    }

    _backward() {
        this.logger.info("setting motor to backward")
        if (wpi) {
            wpi.pwmWrite(this.servoPin, 100)
        }
        this.emit("motorBackward")
    }

    async _limitTriggered() {
        let val = wpi.digitalRead(this.limitPin)
        if (val == wpi.LOW) {
            await this._delay(100)
            let val2 = wpi.digitalRead(this.limitPin)
            if (val == val2) {
                return val == wpi.LOW
            }
        }
        return false
    }

    _stop() {
        this.logger.info("stopping motor")
        if (wpi) {
            wpi.pwmWrite(this.servoPin, 0)
        }
        this.emit("motorStop")
    }

    async _waitForMove() {
        this.logger.info("waiting for move")
        const self = this
        if (await self._limitTriggered()) {
            while (await self._limitTriggered()) {
            }
        }
        while (!await self._limitTriggered()) {
        }
    }

    async _dispenseOnce() {
        this.logger.info("dispensing once")
        if (wpi) {
            this._forward()
            await this._waitForMove()
            this._stop()
        }
    }

    async dispense(cups) {
        this.logger.info(`dispensing ${cups} cups`)
        let iterations = cups / this.cupsToClicks
        for (var i = 0; i < iterations; i++) {
            await this._dispenseOnce()
        }
    }
}

class Feeder extends EventEmitter {
    constructor(url, feederId) {
        super()
        this.logger = new winston.createLogger(myWinstonOptions)
        this.logger.info(`attempting to connect to broker [${url}]`)
        this.mqtt = mqtt.connect(url, {clientId:`feeder-client${feederIdConst}`})
        this.initialized = false
        this.id = feederId
        this.topics = {
            "FEED": `/feeder/${this.id}/feed`,
            "FEED_COMPLETE": `/feeder/${this.id}/feedComplete`,
            "FEED_FAILED": `/feeder/${this.id}/feedFailed`,
            "NEED_INIT": `/feeder/${this.id}/needInit`,
            "CLIENT_INIT": `/feeder/${this.id}/clientInit`,
            "STARTUP": `/feeder/${this.id}/startup`,
            "HEARTBEAT": `/feeder/${this.id}/heartbeat`,
            "SET_SCHEDULE": `/feeder/${this.id}/setSchedule`,
            "SCHEDULE_SET": `/feeder/${this.id}/scheduleSet`

        }
        this.dispenser = new Dispenser(18, 23, 0.25)
        setInterval(() => {
            this.heartbeat()
        }, 5000)
        this.jobs = []
        var self = this
        this.mqtt.on("connect", () => {
            self.logger.info("connected to broker")
            self.startup()
        })

    }

    async saveState() {
        logger.info("saving config")
        var simplified = this.jobs.map((job) => {
            return {
                scheduleId: job.name,
                hour: job.hour,
                minute: job.minute,
                cups: job.cups
            }
        })
        let j = JSON.stringify({feederId: this.id, schedule: simplified})
        return new Promise((resolve, reject) => {
            fs.writeFile("config.json", j, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    }

    async loadState() {
        return new Promise((resolve, reject) => {
            fs.access("config.json", fs.F_OK, (err) => {
                if (err) {
                    reject(err)
                } else {
                    fs.readFile("config.json", "utf8", (err, data) => {
                        if (err) {
                            reject(err)
                        }
                        let j = JSON.parse(data)
                        logger.info("read below config")
                        logger.info(j)
                        this.setSchedule(j.schedule)
                        this.initialized = true
                        resolve()
                    })
                }
            })
        })
    }
    async emit(topic, payload) {
        payload.msgId = uuid()
        await this.mqtt.publish(topic, JSON.stringify(payload)) 
    }
    async startup() {
        await this.setupIO()
        try {
            await this.loadState()
        } catch (err) {
            logger.info("could not read config")
            logger.info("uninitialized, asking server for config")
            this.emit(this.topics.NEED_INIT, this.defaultMeta())
        }
        this.emit(this.topics.STARTUP, this.defaultMeta())
        this.heartbeat()
    }

    defaultMeta(obj) {
        const self = this
        return {
            ...obj,
            ...{
                feederId: this.id,
            }
        }
    }

    async setupIO() {
        var self = this
        await self.mqtt.subscribe(this.topics.FEED)
        await self.mqtt.subscribe(this.topics.SET_SCHEDULE)
        await self.mqtt.subscribe(this.topics.CLIENT_INIT)

        self.mqtt.on("message", async (topic, message) => {

            var msg = JSON.parse(message)
            if (topic == this.topics.FEED) {
                self.logger.info("Got feed request")
                self.feed(msg.cups)
            } else if (topic == this.topics.SET_SCHEDULE) {
                self.logger.info("Got schedule set request")
                self.setSchedule(msg)
            } else if (topic == this.topics.CLIENT_INIT) {
                logger.info("got init config from server")
                await this.setSchedule(msg.schedule)
                await this.saveState()
                await this.loadState()
            }
        })
    }

    async setSchedule(schedules) {
        const self = this
        self.jobs.forEach((oldJob) => {
            oldJob.job.cancel()
        })
        self.jobs = []
        schedules.forEach((sched) => {
            var j = schedule.scheduleJob(sched.scheduleId || uuidv4(),
                {
                    rule: `${sched.minute} ${sched.hour} * * *`,
                    tz: 'UTC'
                }, function () {
                    self.feed(sched.cups)
                });
            self.jobs.push({
                job: j,
                scheduleId: j.name,
                nextInvocation: j.nextInvocation(),
                hour: sched.hour,
                minute: sched.minute,
                cups: sched.cups
            })
        })
        await this.emit(this.topics.SCHEDULE_SET, this.defaultMeta({jobs:this.jobs}))
        this.saveState()
    }

    async feed(cups) {
        try {
            await this.dispenser.dispense(cups)
            this.emit(this.topics.FEED_COMPLETE, this.defaultMeta({feedingId: uuidv4(), cups: cups}))
            this.emit(this.topics.FEEDING_COMPLETE, this.defaultMeta({feedingId: uuidv4(), cups: cups}))
        } catch (e) {
            this.logger.error("feeding failed")
            this.emit(this.topics.FEED_FAILED, this.defaultMeta())
        }
    }

    async heartbeat() {
        if (! this.initialized) {
            logger.info("uninitialized, asking server for config")
            await this.emit(this.topics.NEED_INIT, this.defaultMeta())
        }
        let hb = this.defaultMeta({
            jobs: this.jobs
        })

        await this.emit(this.topics.HEARTBEAT, hb)
        this.logger.info("sent heartbeat")
        this.logger.info(hb)


    }
}

const feederIdConst = process.env['FEEDER_ID']

const myWinstonOptions = {
    format: winston.format.combine(winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }), winston.format.prettyPrint()),
    defaultMeta: {
        component: "feeder-client",
        id: feederIdConst
    },
    transports: [new winston.transports.Console()]
}
const logger = new winston.createLogger(myWinstonOptions)

var wpi = null;
try {
    wpi = require('node-wiring-pi')
} catch (e) {
    logger.info("node-wiring-pi not found, running without")
}

const schedule = require('node-schedule');
const { ExceptionHandler } = require("winston");



console.log(process.env)
var f = new Feeder(`mqtt://${process.env.MQTT_BROKER_HOST}`, feederIdConst)

