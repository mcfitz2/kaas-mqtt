from gmqtt import Client as MQTTClient
import os
import topics, json, uuid
import asyncio, signal
import docker, sys
docker_client = docker.from_env()
FEEDER_ID = os.environ["FEEDER_ID"]
STOP = asyncio.Event()



def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe(topics.UPDATE_SERVICE.topic, qos=2)

def handle_old_instances():
    try:
        old_instance = docker_client.containers.get("kaas-agent-old")
        old_instance.stop()
        old_instance.remove()
    except Exception as e:
        print(e)


    
def update_service(client, service, task_id):
    print(f"attempting to update [{service}]")
    self_update = service == "kaas-agent"
        
    container = docker_client.containers.get(service)
    current_image = container.image
    current_tag = container.attrs['Config']['Image']
    latest_image = docker_client.images.pull(current_tag)
    topic = topics.SERVICE_UPDATE_CHECK.format({"feederId":FEEDER_ID, "service":service})
    publish(client, topic, {"service":service, "status":"checking", "previous":current_image.id, "current":latest_image.id, "task_id":task_id})
    print(f"Current Image: {current_image.id}")
    print(f"Latest  Image: {latest_image.id}")
    if latest_image.id == current_image.id:
        topic = topics.SERVICE_UP_TO_DATE.format({"feederId":FEEDER_ID, "service":service})
        publish(client, topic, {"service":service, "status":"up_to_date", "previous":current_image.id, "current":latest_image.id, "task_id":task_id})
    else:
        print("Images don't match, recreating")
        env_vars = container.attrs['Config']['Env']
        labels = container.attrs['Config']['Labels']
        container.rename(service+"_old")
        new_container = docker_client.containers.create(current_tag, detach = True, environment = env_vars, labels = labels, volumes_from = [container.id], name = service)
        print(f"Created new container {new_container.id}")
        if not self_update:
            print(f"Stopping container {container.id}")
            topic = topics.SERVICE_STOPPED.format({"feederId":FEEDER_ID, "service":service})
            publish(client, topic, {"service":service, "status":"stopped", "task_id":task_id})
            container.stop()
            container.remove()
        print(f"starting container {new_container.id}")
        new_container.start()
        topic = topics.SERVICE_STARTED.format({"feederId":FEEDER_ID, "service":service})
        publish(client, topic, {"service":service, "status":"started", "task_id":task_id})
        topic = topics.SERVICE_UPDATED.format({"feederId":FEEDER_ID, "service":service})
        publish(client, topic, {"service":service, "status":"updated", "previous":current_image.id, "current":latest_image.id, "task_id":task_id})
        if self_update:
            sys.exit(0)
        

def on_message(client, topic, payload, qos, properties):
    if topics.UPDATE_SERVICE.matches(topic):
        topic_params = topics.UPDATE_SERVICE.parse_topic(topic)
        if topic_params['feederId'] == FEEDER_ID:
            print("Got individual feeder update command")
            payload = json.loads(payload)
            update_service(client, payload['service'], payload.get('task_id', "NO_TASK"))
        # elif topic_params['feederId'] == "all":
        #     print("Got fleetwide feeder update command")
        #     payload = json.loads(payload)
        #     if type(payload['service']) == type(list):
        #         for service in payload['service']:
        #             update_service(client, service, task_id)
        #    else:
                #update_service(client, payload['service'])
        else:
            print("ignoring command for another feeder")

def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()
def publish(client, topic, payload):
    payload['msgId'] = str(uuid.uuid4())
    client.publish(topic, json.dumps(payload))

async def main(broker_host):
    client = MQTTClient("agent")

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)
    await STOP.wait()
    await client.disconnect()

if __name__ == '__main__':
    handle_old_instances()
    loop = asyncio.get_event_loop()

    host = os.environ['MQTT_BROKER_HOST']

    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    loop.run_until_complete(main(host))

