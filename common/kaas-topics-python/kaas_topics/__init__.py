import re
class MQTTTopic(object):
    def __init__(self, raw_topic):
        self.raw_topic = raw_topic
        self.regex = self.parse_raw(self.raw_topic)
        self.topic = self.make_clean_topic(self.raw_topic)
    def make_clean_topic(self, raw_topic):
        def handle_token(token):
            if token.startswith('+'):
                return '+'
            elif token.startswith('#'):
                return '#'
            else:
                return token
        return '/'.join([handle_token(t) for t in raw_topic.split('/')])
    def matches(self, topic):
        return self.regex.match(topic)
    def parse_topic(self, topic):
        match = self.matches(topic)
        if match:
            return match.groupdict()
    def parse_raw(self, raw_topic):
        reg_tokens = []
        for token in raw_topic.split('/'):
            if token.startswith("+"):
                group_name = token[1:]
                reg = f"(?P<{group_name}>.+/?)"
                reg_tokens.append(reg)
            else:
                reg_tokens.append(token)
        return re.compile('/'.join(reg_tokens))
    def format(self, data):
        reg_tokens = []
        for token in self.raw_topic.split('/'):
            if token.startswith("+"):
                group_name = token[1:]
                reg_tokens.append(data[group_name])
            else:
                reg_tokens.append(token)
        return '/'.join(reg_tokens)


UPDATE_SERVICE  = MQTTTopic("/feeder/+feederId/services/+service/update")
FLEET_UPDATE_SERVICE  = MQTTTopic("/feeder/all/services/+service/update")
SERVICE_UPDATED = MQTTTopic("/feeder/+feederId/services/+service/updated")
SERVICE_STOPPED = MQTTTopic("/feeder/+feederId/services/+service/stopped")
SERVICE_STARTED = MQTTTopic("/feeder/+feederId/services/+service/stopped")
SERVICE_UP_TO_DATE = MQTTTopic("/feeder/+feederId/services/+service/up_to_date")
RECORDING_STORED = MQTTTopic("/feeder/+feederId/feeding/+feedingId/stored")
RECORDING_CONVERTED = MQTTTopic("/feeder/+feederId/feeding/+feedingId/converted")