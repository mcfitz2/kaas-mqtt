const mqtt_regex = require("mqtt-regex");

module.exports = {
    "FEED": mqtt_regex(`/feeder/+feederId/feed`),
    "FEED_COMPLETE": mqtt_regex(`/feeder/+feederId/feedComplete`),
    "FEED_FAILED": mqtt_regex(`/feeder/+feederId/feedFailed`),
    "NEED_INIT": mqtt_regex(`/feeder/+feederId/needInit`),
    "CLIENT_INIT": mqtt_regex(`/feeder/+feederId/clientInit`),
    "STARTUP": mqtt_regex(`/feeder/+feederId/startup`),
    "HEARTBEAT": mqtt_regex(`/feeder/+feederId/heartbeat`),
    "SCHEDULE_SET": mqtt_regex(`/feeder/+feederId/scheduleSet`),
    "SET_SCHEDULE": mqtt_regex(`/feeder/+feederId/setSchedule`),
    "LOST": mqtt_regex(`/feeder/+feederId/lost`),
    "FOUND": mqtt_regex(`/feeder/+feederId/found`),
    "FEEDING_COMPLETE":mqtt_regex(`/feeder/+feederId/feeding/+feedingId/complete`),
    "FEEDING_FAILED":mqtt_regex(`/feeder/+feederId/feeding/+feedingId/failed`),
    "LOST": mqtt_regex(`/feeder/+feederId/lost`),
    "FOUND": mqtt_regex(`/feeder/+feederId/found`),
    "RECORDING_STORED":mqtt_regex(`/feeder/+feederId/feeding/+feedingId/stored`),
    "FEEDING_RECORDED":mqtt_regex(`/feeder/+feederId/feeding/+feedingId/recorded`)
}